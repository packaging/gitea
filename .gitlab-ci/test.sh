#!/bin/bash

set -e

VERSION=$(echo "${CI_COMMIT_TAG#*v}" | cut -d'+' -f1)
PATCHLEVEL=$(echo "${CI_COMMIT_TAG}" | cut -d'+' -f2)
export VERSION PATCHLEVEL

if [ ! -f /etc/init.d/gitea ]; then
    cp -v "${CI_PROJECT_DIR}"/.gitlab-ci/init /etc/init.d/gitea
fi

start_gitea() {
  /etc/init.d/gitea start
  sleep 10
  if ! /etc/init.d/gitea status | grep 'gitea is running'; then
    cat /var/lib/gitea/log/gitea.log
  fi
}

restart_gitea() {
  /etc/init.d/gitea stop
  start_gitea
}

sleep 5

apt-get update
apt-get -y install curl jq
