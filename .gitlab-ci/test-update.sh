#!/bin/bash

. "${CI_PROJECT_DIR}/.gitlab-ci/test.sh"

curl -sL -o /etc/apt/trusted.gpg.d/morph027-gitea.asc https://packaging.gitlab.io/gitea/gpg.key
echo "deb [arch=amd64] https://packaging.gitlab.io/gitea gitea main" > /etc/apt/sources.list.d/morph027-gitea.list
apt-get update
apt-get -y install gitea
install -o gitea -g gitea -m 644 "/${CI_PROJECT_DIR}/.gitlab-ci/app.ini" /etc/gitea/app.ini
start_gitea
curl -s localhost:3000/api/v1/version | jq -e '.version'
apt-get -y --allow-downgrades install "${CI_PROJECT_DIR}/gitea_${VERSION}+${PATCHLEVEL}_amd64.deb"
restart_gitea
curl -s localhost:3000/api/v1/version | jq -e '.version'
