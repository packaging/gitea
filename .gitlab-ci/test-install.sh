#!/bin/bash

. "${CI_PROJECT_DIR}/.gitlab-ci/test.sh"

apt-get -y install "${CI_PROJECT_DIR}/gitea_${VERSION}+${PATCHLEVEL}_amd64.deb"
start_gitea
