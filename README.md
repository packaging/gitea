# [Gitea](https://gitea.io) Debian/Ubuntu Packages

Packages are built using [fpm](https://github.com/jordansissel/fpm/) by pushing released tags and repo is created using [Gitlabs static pages](https://morph027.gitlab.io/blog/repo-hosting-using-gitlab-pages/).

Minimum install and update tests are in place to prevent upload of broken packages.

### Add repo signing key to apt

```
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-gitea.asc https://packaging.gitlab.io/gitea/gpg.key
```

### Add repo to apt

```
echo "deb https://packaging.gitlab.io/gitea gitea main" | sudo tee /etc/apt/sources.list.d/morph027-gitea.list
```

### Install

```
sudo apt-get update
sudo apt-get install gitea morph027-keyring
```

### Start

```
systemctl enable --now gitea
```

## Extras

### unattended-upgrades

To enable automatic upgrades using `unattended-upgrades`, just add the following config file:

```bash
cat > /etc/apt/apt.conf.d/50gitea <<EOF
Unattended-Upgrade::Allowed-Origins {
	"morph027:gitea";
};
EOF
```
