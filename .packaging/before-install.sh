# https://docs.gitea.io/en-us/install-from-binary/

getent passwd gitea >/dev/null 2>&1 || adduser \
  --system \
  --shell /bin/bash \
  --gecos 'Gitea' \
  --group \
  --disabled-password \
  --home /var/lib/gitea \
  gitea

if [ ! -d /etc/gitea ]; then
  install -d -o gitea -g gitea -m 770 /etc/gitea
fi
