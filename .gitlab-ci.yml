---
stages:
  - package
  - test
  - repo
  - pages

.vars: &vars
  NAME: gitea
  URL: "https://gitea.io"
  DESCRIPTION: "A painless self-hosted Git service."
  ARTIFACTS: "usr"

.package: &package
  stage: package
  image: golang:latest
  script:
    - export VERSION=$(echo ${CI_COMMIT_TAG#*v} | cut -d'+' -f1)
    - export PATCHLEVEL=$(echo ${CI_COMMIT_TAG} | cut -d'+' -f2)
    - apt-get -qq update
    - apt-get -qqy install ruby-dev ruby-ffi curl file
    - gem install fpm
    - mkdir -p "${CI_PROJECT_DIR}/package_root/usr/bin"
    - echo "https://dl.gitea.com/gitea/${VERSION}/gitea-${VERSION}-linux-${ARCH}"
    - curl -sL -o
        "${CI_PROJECT_DIR}/package_root/usr/bin/gitea"
        "https://dl.gitea.com/gitea/${VERSION}/gitea-${VERSION}-linux-${ARCH}"
    - file
        --mime-type
        --brief
        "${CI_PROJECT_DIR}/package_root/usr/bin/gitea"
        | grep -q 'application/x-executable'
    - chmod +x "${CI_PROJECT_DIR}/package_root/usr/bin/gitea"
    - gem install fpm
    - fpm
        --architecture "${ARCH}"
        --input-type dir
        --output-type deb
        --package "${CI_PROJECT_DIR}/gitea_${VERSION}+${PATCHLEVEL}_${ARCH}.deb"
        --name "${NAME}"
        --version "${VERSION}+${PATCHLEVEL}"
        --description "${DESCRIPTION}"
        --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>"
        --url "${URL}"
        --depends git
        --deb-recommends morph027-keyring
        --deb-systemd "${CI_PROJECT_DIR}/.packaging/${NAME}.service"
        --deb-activate-noawait /etc/init.d
        --prefix=/
        --before-install "${CI_PROJECT_DIR}/.packaging/before-install.sh"
        --chdir "${CI_PROJECT_DIR}/package_root"
        $ARTIFACTS
  artifacts:
    paths:
      - $CI_PROJECT_DIR/*.deb
    expire_in: 7 days

.manual: &manual
  when: manual
  only:
    - tags

.trigger: &trigger
  when: delayed
  start_in: 2 hours
  only:
    - triggers

gitea-amd64-tag:
  <<: [*package, *manual]
  variables:
    <<: *vars
    ARCH: amd64

gitea-amd64-trigger:
  <<: [*package, *trigger]
  variables:
    <<: *vars
    ARCH: amd64

gitea-arm64-tag:
  <<: [*package, *manual]
  variables:
    <<: *vars
    ARCH: arm64

gitea-arm64-trigger:
  <<: [*package, *trigger]
  variables:
    <<: *vars
    ARCH: arm64

.test-install: &test-install
  stage: test
  image: ubuntu:noble
  script:
    - ./.gitlab-ci/test-install.sh

.test-update: &test-update
  stage: test
  image: ubuntu:noble
  script:
    - ./.gitlab-ci/test-update.sh

test-install-trigger:
  <<: *test-install
  needs:
    - gitea-amd64-trigger
  only:
    - trigger

test-install-tag:
  <<: *test-install
  needs:
    - gitea-amd64-tag
  only:
    - tags

test-update-trigger:
  <<: *test-update
  needs:
    - gitea-amd64-trigger
  only:
    - trigger

test-update-tag:
  <<: *test-update
  needs:
    - gitea-amd64-tag
  only:
    - tags

.repro: &repo
  cache:
    untracked: true
    paths:
      - $CI_PROJECT_DIR/.repo
    key: repo
  stage: repo
  image: registry.gitlab.com/packaging/utils:latest
  script:
    - /deb.sh gitea

repo-apt-tag:
  <<: [*repo]
  needs:
    - gitea-amd64-tag
    - gitea-arm64-tag
    - test-install-tag
    - test-update-tag
  rules:
    - if: $CI_COMMIT_TAG && $CI_PIPELINE_SOURCE != "trigger"


repo-apt-trigger:
  <<: [*repo]
  needs:
    - gitea-amd64-trigger
    - gitea-arm64-trigger
    - test-install-trigger
    - test-update-trigger
  rules:
    - if: $CI_COMMIT_TAG && $CI_PIPELINE_SOURCE == "trigger"

pages:
  cache:
    untracked: true
    paths:
      - $CI_PROJECT_DIR/.repo
    key: repo
    policy: pull
  stage: pages
  image: ubuntu:focal
  needs:
    - job: repo-apt-tag
      optional: true
    - job: repo-apt-trigger
      optional: true
  script:
    - mkdir $CI_PROJECT_DIR/public
    - cp -rv
        $CI_PROJECT_DIR/.repo/deb/gpg.key
        $CI_PROJECT_DIR/.repo/deb/gitea/gitea/{dists,pool}
        $CI_PROJECT_DIR/public/
  artifacts:
    paths:
      - $CI_PROJECT_DIR/public
    expire_in: 1 day
  only:
    - tags
    - triggers
